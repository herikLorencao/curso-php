<div class="titulo">Desafio Tabela #02</div>

<form action="#" method="post">
    <label for="linhas">Qtd. Linhas: </label>
    <input type="number" name="linhas" id="linhas">
    <label for="colunas">Qtd. Colunas: </label>
    <input type="number" name="colunas" id="colunas">
    <button>Gerar</button>
</form>

<table>

    <?php
        $qtdLinhas = intval($_POST['linhas']) ?? 0;
        $qtdColunas = intval($_POST['colunas']) ?? 0;

        $numero = 1;

        for ($i = 0; $i < $qtdLinhas; $i++) {
            echo '<tr>';
            for ($j = 0; $j < $qtdColunas; $j++) {
                echo "<td>$numero </td>";
                $numero++;
            }
            echo '</tr>';
        }
    ?>

</table>

<style>
    form>input,
    form>button {
        font-size: 1.3rem;
    }

    table {
        border: 1px solid #444;
        border-collapse: collapse;
        margin: 20px 0px;
    }

    table>tr {
        border: 1px solid #444;
    }

    table td {
        padding: 10px 20px;
    }

    [listra] {
        background-color: #eee;
    }
</style>