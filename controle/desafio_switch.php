<div class="titulo">Desafio Switch</div>

<form action="#" method="post">
    <input type="text" name="param">
    <select name="conversao">
        <option value="km_milha">KM > Milha</option>
        <option value="milha_km">Milha > KM</option>
        <option value="metros_km">Metros > KM</option>
        <option value="km_metros">KM > Metros</option>
        <option value="celsius_fahrenheit">Celsius > Fahrenheit</option>
        <option value="fahrenheit_celsius">Fahrenheit > Celsius</option>
    </select>
    <button>Calcular</button>
</form>

<style>
    form>* {
        font-size: 1.5rem;
    }
</style>

<?php
$valor = $_POST['param'] ?? 0;
$unidade = $_POST['conversao'];
$resultado = 0;

switch ($unidade) {
    case 'km_milha':
        $resultado = $valor * 1.60934;
        break;
    case 'milha_km':
        $resultado = $valor / 1.60934;
        break;
    case 'metros_km':
        $resultado = $valor / 1000;
        break;
    case 'km_metros':
        $resultado = $valor * 1000;
        break;
    case 'celsius_fahrenheit':
        $resultado = ($valor * 9/5) + 32;
        break;
    case 'fahrenheit_celsius':
        $resultado = ($valor - 32) * 5/9;
        break;
    default:
        echo '<br>Nenhuma opção selecionada!';
}

$resultado = number_format($resultado, 2, ',', '.');
echo "<br>Resultado = $resultado";