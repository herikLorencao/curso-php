<div class="titulo">Desafio Palindromo</div>

<?php
function ehPalindromo($palavra) {
    $tamanho = strlen($palavra);
    $palavraInvertida = '';

    for ($i = $tamanho - 1; $i >= 0; $i--) {
        $palavraInvertida .= "{$palavra[$i]}";
    }

    return $palavra === $palavraInvertida ? 'Sim' : 'Não';
}

function ehPalindromo2($palavra){
    return $palavra === strrev($palavra) ? 'Sim' : 'Não';
}

$palindromo = ehPalindromo('ana');
echo "É palindromo? {$palindromo} <br>";

$palindromo = ehPalindromo2('arara');
echo "É palindromo? {$palindromo}";