<div class="titulo">Desafio Recursão</div>

<?php
$array = [1, 2, [3, 4, 5], 6, [7, [8, 9]], 10];

function percorrerArray($array, $nivel = '>') {
    foreach($array as $item) {
        if (is_array($item)) {
            percorrerArray($item, $nivel . $nivel[0]);
        } else {
            echo "$nivel $item <br>";
        }
    }
}

percorrerArray($array);