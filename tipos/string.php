<div class="titulo">Tipo String</div>

<?php
echo 'Sou uma String';
echo '<br>';
var_dump("Eu também");
echo '<br>';

// concatenação
echo("Nós também" . ' somos');
echo '<br>', "Também aceito", " vírgulas";  // só concatenar com , no echo

echo '<br>';
echo "'Teste' " . '"Teste" ' . '\'Teste\' ' . "\"Teste\" " . "\\";

print("<br>Também existe a função print");
print "<br>Também existe a função print";

// Algumas funções
echo '<br>' . strtoupper('maximizado');
echo '<br>' . strtolower('MINIMAZADO');
echo '<br>' . ucfirst('só a primeira letra');
echo '<br>' . ucwords('todas as palavras');
echo '<br>' . strlen('Quantas letras?');
// echo '<br>' . mb_strlen("Eu também", "utf-8");  // para resolver problemas de encoding
echo '<br>' . substr('Só uma parte mesmo', 7, 6); // ][
echo '<br>' . str_replace('isso', 'aquilo', 'Trocar isso isso');