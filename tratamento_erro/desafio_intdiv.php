<?php

namespace desafio;

use Exception;
use DivisionByZeroError;

?>

<div class="titulo">Desafio Intdiv</div>

<?php
class DivisaoNaoInteiraException extends Exception
{
    function __construct($message, $code = 0, $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}

function intdiv($divident, $divisor)
{
    if ($divisor === 0) {
        throw new DivisionByZeroError('Não é possível dividir por zero');
    }
    if ($divident % $divisor > 0) {
        throw new DivisaoNaoInteiraException('O resultado não é inteiro');
    }
    return $divident / $divisor;
}

echo intdiv(8, 2);

try {
    echo '<br>';
    intdiv(8, 3);
    echo '<br>';
} catch (DivisaoNaoInteiraException $e) {
    echo $e->getMessage();
}

try {
    echo '<br>';
    intdiv(8, 0);
    echo '<br>';
} catch (DivisionByZeroError $e) {
    echo $e->getMessage();
}

echo '<br>' . \intdiv(8, 3);
