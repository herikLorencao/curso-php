<?php
session_start();
session_destroy();
unset($_COOKIE['usuario']); // limpa variavel
setcookie('usuario', '');   // limpa cookie no browser
header('Location: login.php');
