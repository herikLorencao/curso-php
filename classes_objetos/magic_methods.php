<div class="titulo">Métodos Mágicos</div>

<?php
class Pessoa {
    public $nome;
    public $idade;

    function __construct($nome, $idade) {
        echo 'Construtor invocado!<br>';
        $this->nome = $nome;
        $this->idade = $idade;
    }

    function __destruct() {
        echo 'E morreu! <br>';
    }

    public function __toString() {
        return "{$this->nome} tem {$this->idade} anos.";
    }

    public function apresentar() {
        echo $this . '<br>';
    }

    public function __get($atributo) {
        echo "Lendo atributo não declarado: {$atributo}<br>";
    }

    public function __set($atributo, $valor) {
        echo "Alterando atributo não declarado: {$atributo}/{$valor}<br>";
    }

    public function __call($metodo, $params) {
        echo "Tentando executar método '${metodo}'";
        echo " , com os parametros: ";
        print_r($params);
    }
}

$pessoa = new Pessoa('Ricardo', 40);
$pessoa->apresentar();  // chamando o toString
echo $pessoa, '<br>';   // chamando o toString
$pessoa->nome = 'Reinaldo';

// chama o método diretamente sem o __call
$pessoa->apresentar();

$pessoa->nomeCompleto = 'Muito Legal!!!';   // __set
$pessoa->nomeCompleto;  // __get

// acessa o atributo diretamente sem o __get
echo $pessoa->nome;

// __call pq o método não existe no objeto
$pessoa->exec(1, 'teste', true, [1, 2, 3]);

$pessoa = null; // __destruct