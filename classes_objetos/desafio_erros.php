<div class="titulo">Desafio Erros</div>

<?php
interface Template {
    function metodo1();
    function metodo2($parametro);
}

abstract class ClasseAbstrata implements Template {
    public function metodo3() {
        echo "Estou funcionando";
    }
}

class Classe extends ClasseAbstrata {
    public function __construct($parametro) {

    }
    
    public function metodo1() {
        echo "Estou funcionando";
    }
    
    public function metodo2($parametro) {
        echo "Estou funcionando, com o parâmetro $parametro";
    }
}

$exemplo = new Classe('parametro');
$exemplo->metodo3();